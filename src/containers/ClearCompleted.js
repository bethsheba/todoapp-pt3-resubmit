import React from "react";
import { connect } from "react-redux";
import { clearCompleted } from "../actions";

const ClearCompleted = ({ clearCompleted, children }) => {
  return (
    <button onClick={clearCompleted} className="clear-completed">
      {children}
    </button>
  );
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  clearCompleted: () => dispatch(clearCompleted(ownProps.completed)),
});

export default connect(
  null, //i dont want anything from the redux store, otherword dont need any state
  mapDispatchToProps
)(ClearCompleted);
