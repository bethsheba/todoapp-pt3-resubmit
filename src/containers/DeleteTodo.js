import React from "react";
import { connect } from "react-redux";
import { deleteTodo } from "../actions";

const DeleteTodo = ({ deleteTodo }) => {
  return <button className="destroy" onClick={deleteTodo}></button>;
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  deleteTodo: () => dispatch(deleteTodo(ownProps.id)),
});

export default connect(
  null, //i dont want anything from the redux store, otherword dont need any state
  mapDispatchToProps
)(DeleteTodo);
