import React from "react";
import { connect } from "react-redux";
import { addTodo } from "../actions";

const AddTodo = ({ dispatch }) => {
  let input;

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <form
          onSubmit={(event) => {
            event.preventDefault();

            if (!input.value.trim()) {
              return;
            }
            dispatch(addTodo(input.value));
            input.value = "";
          }}
        >
          <input
            className="new-todo"
            type="text"
            placeholder="What needs to be done?"
            ref={(el) => (input = el)}
            autoFocus
          />
        </form>
      </header>
    </section>
  );
};

export default connect()(AddTodo);
