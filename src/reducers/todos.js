const todos = (state = [], action) => {
  console.log(action.type);
  switch (action.type) {
    case "ADD_TODO":
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          completed: false,
          userId: action.userId,
        },
      ];

    case "TOGGLE_TODO":
      return state.map((todo) =>
        todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
      );

    case "DELETE_TODO":
      console.log("deleteTodo called ", action);
      return state.filter((todo) => todo.id !== action.id);

    case "CLEAR_COMPLETED_TODOS":
      console.log("clear todos called ", action);
      return state.filter((todo) => {
        return !todo.completed;
      });

    default:
      return state;
  }
};

export default todos;
