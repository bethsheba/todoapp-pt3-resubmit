import React from "react";
import DeleteTodo from "../containers/DeleteTodo";

const Todo = ({ id, onClick, completed, text }) => (
  <li style={{ textDecoration: completed ? "line-through" : "none" }}>
    <input
      className="todoapp"
      type="checkbox"
      checked={completed}
      onClick={onClick}
    />
    <DeleteTodo id={id} />
    {text}
  </li>
);

export default Todo;
