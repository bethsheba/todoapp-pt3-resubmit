import React from "react";
import AddTodo from "../containers/AddTodo";
import VisibleTodoList from "../containers/VisibleTodoList";
import Footer from "./Footer";
import { BrowserRouter } from "react-router-dom";

const App = () => (
  <BrowserRouter>
    <AddTodo />
    <VisibleTodoList />
    <Footer />
  </BrowserRouter>
);

export default App;
