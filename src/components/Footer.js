import React from "react";
import FilterLink from "../containers/FilterLink";
import { VisibilityFilters, clearCompleted } from "../actions";
import ClearCompleted from "../containers/ClearCompleted";


const Footer = (id) => (
  <section>
    <footer className="footer">
      <span className="todo-count">item(s) left</span>
      <ul className="filters">
        <li>
          <FilterLink
            filter={VisibilityFilters.SHOW_ALL}
            exact="true"
            to="/"
            activeClassName="selected"
          >
            All
          </FilterLink>
        </li>
        <li>
          <FilterLink
            filter={VisibilityFilters.SHOW_ACTIVE}
            exact
            to="/active"
            activeClassName="selected"
          >
            Active
          </FilterLink>
        </li>
        <li>
          <FilterLink
            filter={VisibilityFilters.SHOW_COMPLETED}
            to="/completed"
            activeClassName="selected"
          >
            Completed
          </FilterLink>
        </li>
      </ul>
 
    
    <ClearCompleted  onClick={clearCompleted(id)}>Clear Completed </ClearCompleted> 
    
    </footer>
  </section>
);

export default Footer;

