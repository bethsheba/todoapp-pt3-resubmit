import React from "react";
import { NavLink as Linked} from 'react-router-dom';

const Link = ({ children, ...props }) => (
  <Linked {...props} >
    {children}
  </Linked>
);

export default Link;
