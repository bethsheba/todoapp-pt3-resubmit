let nextTodoID = 0.01;

export const addTodo = (text) => ({
  type: "ADD_TODO",
  id: nextTodoID++,
  text,
});

export const toggleTodo = (id) => ({
  type: "TOGGLE_TODO",
  id,
});

export const VisibilityFilters = {
  SHOW_ALL: "SHOW_ALL",
  SHOW_COMPLETED: "SHOW_COMPLETED",
  SHOW_ACTIVE: "SHOW_ACTIVE",
};

export const setVisibilityFilter = (filter) => ({
  type: "SET_VISIBILITY_FILTER",
  filter,
});


export const deleteTodo = (id) => {
  console.log("Dispatching action DELETE_TODO. got id", id)
  return {
    type: "DELETE_TODO",
    id,
  };
};

export const clearCompleted = (id) => {
  console.log("Dispatching action clear todos. got id", id)
  return {
    type: "CLEAR_COMPLETED_TODOS",
  };
};

